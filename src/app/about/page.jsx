"use client";
import Brain from "../../components/brain";
import { motion, useInView, useScroll } from "framer-motion";
import Image from "next/image";
import { useRef } from "react";
import profile from "/public/Profile.png";
import Link from "next/link";
import Logo from "./logo";
import GitiNext from "../../../public/Giti Next-02 4.png";

const AboutPage = () => {
  const containerRef = useRef();

  const { scrollYProgress } = useScroll({ container: containerRef });

  const skillRef = useRef();
  // const isSkillRefInView = useInView(skillRef, {once:true});
  const isSkillRefInView = useInView(skillRef, { margin: "-100px" });

  const experienceRef = useRef();
  const isExperienceRefInView = useInView(experienceRef, { margin: "-100px" });

  return (
    <motion.div
      className="h-full"
      initial={{ y: "-200vh" }}
      animate={{ y: "0%" }}
      transition={{ duration: 1 }}
    >
      {/* CONTAINER */}
      <div className="h-full overflow-scroll lg:flex" ref={containerRef}>
        {/* TEXT CONTAINER */}
        <div className="p-4 sm:p-8 md:p-12 lg:p-20 xl:p-48 flex flex-col gap-24 md:gap-32 lg:gap-48 xl:gap-64 lg:w-2/3 lg:pr-0 xl:w-1/2">
          {/* BIOGRAPHY CONTAINER */}
          <div className="flex flex-col gap-12 justify-center">
            {/* BIOGRAPHY IMAGE */}
            <Image
              src={profile}
              alt=""
              width={112}
              height={112}
              className="w-28 h-28 rounded-full object-cover"
            />
            {/* BIOGRAPHY TITLE */}
            <h1 className="font-bold text-2xl">BIOGRAPHY</h1>
            {/* BIOGRAPHY DESC */}
            <p className="text-lg">
              My name is Poria Hosseinzadeh and I live in Iran Tehran. I am a UI
              Designer and full stack Developer who specializes in blockchain
              technology. I enjoy using my skills to help businesses. I have
              bachelor’s degree in business management and MBA certificate. I
              started learning UI/UX & Coding from 2018 through self-study.
            </p>
            {/* BIOGRAPHY QUOTE */}
            <span className="italic">
              Every problem is a gift, without problems we would not grow.
            </span>
            {/* BIOGRAPHY SIGN SVG*/}
            <div className="self-end">
              <svg
                width="98"
                height="84"
                viewBox="0 0 98 84"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M3.64354 74.2753C3.64354 73.4667 3.12243 72.898 3.12243 72.1233C3.12243 71.2466 2.94872 70.3692 2.94872 69.4792C2.94872 65.3833 2.0802 61.4306 2.0802 57.3295C2.0802 50.3837 1.55908 43.4291 1.55908 36.4849C1.55908 28.5266 1.55908 20.5684 1.55908 12.6101"
                  stroke="black"
                  stroke-width="3"
                  stroke-linecap="round"
                />
                <path
                  d="M1.55908 12.6101C1.55908 9.68358 1.78492 6.73911 4.39626 4.96713C7.9275 2.57093 12.8091 1.59019 17.0381 1.66674C19.2864 1.70743 21.4288 2.19524 23.6389 2.53526C24.2947 2.63615 24.945 2.64631 25.4917 3.05638C26.6039 3.89049 27.1047 5.12766 27.4797 6.45327C28.3431 9.50535 28.7759 13.8579 27.4411 16.837C26.1951 19.6178 24.9513 22.5714 22.8862 24.866C19.2857 28.8665 13.9612 31.8914 8.54588 31.8914C7.80192 31.8914 3.08981 32.3473 2.77502 31.7177"
                  stroke="black"
                  stroke-width="3"
                  stroke-linecap="round"
                />
                <path
                  d="M36.1263 22.3376C36.1263 19.7499 36.3054 17.3854 37.4774 15.0227C38.0022 13.9646 38.6659 12.2627 40.0347 12.2627C40.8195 12.2627 41.5498 12.0639 42.0709 12.7066C45.127 16.4758 45.7218 21.7054 46.0951 26.3811C46.8448 35.772 46.2579 45.0692 45.0721 54.3861C44.2978 60.4704 43.271 66.6178 41.3954 72.4611C40.612 74.9016 39.2507 77.3522 37.8634 79.4961C37.3245 80.3289 36.9598 81.0849 36.3 81.7446"
                  stroke="black"
                  stroke-width="3"
                  stroke-linecap="round"
                />
                <path
                  d="M46.3749 46.6563C46.8892 46.4955 47.2277 46.1801 47.7549 46.0483C49.0528 45.7238 50.6014 45.9782 51.9238 45.9518C54.7342 45.8956 57.4752 44.9466 60.2906 44.7455C61.0405 44.6919 61.6562 44.3981 62.4619 44.3981C63.5959 44.3981 64.7061 44.2244 65.8298 44.2244"
                  stroke="black"
                  stroke-width="3"
                  stroke-linecap="round"
                />
                <path
                  d="M73.4728 6.18304C72.1043 9.83231 71.7839 13.7881 70.9348 17.5704C70.0429 21.5435 69.1958 25.6027 68.8793 29.6718C68.7347 31.5305 68.5989 33.3623 68.3389 35.211C67.9811 37.7553 67.3434 40.2965 67.1229 42.8541C66.9733 44.5902 67.1952 46.3602 66.7755 48.0652C66.6562 48.5501 66.6983 49.055 66.6983 49.5513C66.6983 51.0286 66.5083 52.6462 66.7273 54.1063C66.818 54.7114 67.2194 55.1498 67.2194 55.7758C67.2194 56.21 67.2194 56.6443 67.2194 57.0785C67.2194 58.603 67.857 59.3516 68.4161 60.7842C69.1174 62.5814 69.4612 64.5165 70.1724 66.2945C70.5539 67.2482 71.5033 68.285 72.2183 69.0256C72.839 69.6685 73.2792 70.5977 73.9553 71.1486C74.7379 71.7863 75.424 72.5241 76.2328 73.1559C77.2329 73.9372 77.7727 73.9279 79.0217 73.9279C80.4177 73.9279 81.938 73.4032 82.2932 71.8049C82.5325 70.7276 82.9312 69.8115 83.1617 68.7168C83.2894 68.1099 83.2003 67.4245 83.2003 66.806"
                  stroke="black"
                  stroke-width="3"
                  stroke-linecap="round"
                />
                <path
                  d="M91.9369 31.4414C88.3226 32.3019 85.7217 33.2786 82.3367 31.0972C79.83 29.4817 79.5983 26.8418 79.5464 24.0415C79.4918 21.0934 79.455 18.1683 81.8819 16.0762C83.5645 14.6257 85.0759 13.2981 87.3765 13.2981C89.581 13.2981 91.2298 14.9584 92.5024 16.7399C93.5076 18.1472 94.0997 19.4677 94.7395 21.0545C95.1179 21.9928 95.7365 22.8605 95.8704 23.8817C96.1188 25.7758 95.9473 27.8771 95.8704 29.7696C95.6161 36.0317 94.31 42.0374 91.7403 47.7654C90.5055 50.5176 88.7492 52.2029 86.6267 54.2311C84.415 56.3445 81.5558 56.8689 78.6614 57.55"
                  stroke="black"
                  stroke-width="3"
                  stroke-linecap="round"
                />
              </svg>
            </div>
            {/* BIOGRAPHY SCROLL SVG */}
            <motion.svg
              initial={{ opacity: 0.2, y: 0 }}
              animate={{ opacity: 1, y: "10px" }}
              transition={{ repeat: Infinity, duration: 3, ease: "easeInOut" }}
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              width={50}
              height={50}
            >
              <path
                d="M5 15C5 16.8565 5.73754 18.6371 7.05029 19.9498C8.36305 21.2626 10.1435 21.9999 12 21.9999C13.8565 21.9999 15.637 21.2626 16.9498 19.9498C18.2625 18.6371 19 16.8565 19 15V9C19 7.14348 18.2625 5.36305 16.9498 4.05029C15.637 2.73754 13.8565 2 12 2C10.1435 2 8.36305 2.73754 7.05029 4.05029C5.73754 5.36305 5 7.14348 5 9V15Z"
                stroke="#000000"
                strokeWidth="1"
              ></path>
              <path d="M12 6V14" stroke="#000000" strokeWidth="1"></path>
              <path
                d="M15 11L12 14L9 11"
                stroke="#000000"
                strokeWidth="1"
              ></path>
            </motion.svg>
          </div>
          {/* SKILLS CONTAINER */}
          <div className="flex flex-col gap-12 justify-center" ref={skillRef}>
            {/* SKILL TITLE */}
            <motion.h1
              initial={{ x: "-300px" }}
              animate={isSkillRefInView ? { x: 0 } : {}}
              transition={{ delay: 0.2 }}
              className="font-bold text-2xl"
            >
              SKILLS
            </motion.h1>
            {/* SKILL LIST */}
            <motion.div
              initial={{ x: "-300px" }}
              animate={isSkillRefInView ? { x: 0 } : {}}
              className="flex gap-4 flex-wrap"
            >
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                JavaScript
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                TypeScript
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                React.js
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Next.js
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                SCSS
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Tailwind CSS
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                MongoDB
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                MySQL
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                PostgreSQL
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Redux
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Framer Motion
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Three.js
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                WebGL
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Webpack
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Vite
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Jest
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Docker
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Git
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Figma
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                PHP
              </div>
              <div className="rounded p-2 text-sm cursor-pointer bg-black text-white hover:bg-white hover:text-black">
                Laravel
              </div>
            </motion.div>
            {/* SKILL SCROLL SVG */}
            <motion.svg
              initial={{ opacity: 0.2, y: 0 }}
              animate={{ opacity: 1, y: "10px" }}
              transition={{ repeat: Infinity, duration: 3, ease: "easeInOut" }}
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              width={50}
              height={50}
            >
              <path
                d="M5 15C5 16.8565 5.73754 18.6371 7.05029 19.9498C8.36305 21.2626 10.1435 21.9999 12 21.9999C13.8565 21.9999 15.637 21.2626 16.9498 19.9498C18.2625 18.6371 19 16.8565 19 15V9C19 7.14348 18.2625 5.36305 16.9498 4.05029C15.637 2.73754 13.8565 2 12 2C10.1435 2 8.36305 2.73754 7.05029 4.05029C5.73754 5.36305 5 7.14348 5 9V15Z"
                stroke="#000000"
                strokeWidth="1"
              ></path>
              <path d="M12 6V14" stroke="#000000" strokeWidth="1"></path>
              <path
                d="M15 11L12 14L9 11"
                stroke="#000000"
                strokeWidth="1"
              ></path>
            </motion.svg>
          </div>
          {/* EXPERIENCE CONTAINER */}
          <div
            className="flex flex-col gap-12 justify-center pb-48"
            ref={experienceRef}
          >
            {/* EXPERIENCE TITLE */}
            <motion.h1
              initial={{ x: "-300px" }}
              animate={isExperienceRefInView ? { x: "0" } : {}}
              transition={{ delay: 0.2 }}
              className="font-bold text-2xl"
            >
              EXPERIENCE
            </motion.h1>
            {/* EXPERIENCE LIST */}
            <motion.div
              initial={{ x: "-300px" }}
              animate={isExperienceRefInView ? { x: "0" } : {}}
              className=""
            >
              {/* EXPERIENCE LIST ITEM */}
              <div className="flex justify-between h-48">
                {/* LEFT */}
                <div className="w-1/3 ">
                  {/* JOB TITLE */}
                  <div className="bg-white p-3 font-semibold rounded-b-lg rounded-s-lg">
                    Frontend Developer
                  </div>
                  {/* JOB DESC */}
                  <div className="p-3 text-sm italic">
                    I developed Five products for this company : 1.Block chain
                    Node Admin Panel
                    <br />
                    2.Crypto exchange admin panel
                    <br />
                    3.Crypto exchange user panel
                    <br />
                    4.Fast convert web app with internationalization
                    <br />
                    5.WebApp presentation
                  </div>
                  {/* JOB DATE */}
                  <div className="p-3 text-red-400 text-sm font-semibold">
                    Jan 2020 - Jan 2024
                  </div>
                  {/* JOB COMPANY */}
                  <div className="flex ">
                    {" "}
                    <div className="p-1 rounded bg-white text-sm font-semibold w-fit">
                      Exonyx
                    </div>
                    <div className="ml-1 w-6 h-6">
                      <Logo width={26} height={26} />
                    </div>
                  </div>
                </div>
                {/* CENTER */}
                <div className="w-1/6 flex justify-center">
                  {/* LINE */}
                  <div className="w-1 h-full bg-gray-600 rounded relative">
                    {/* LINE CIRCLE */}
                    <div className="absolute w-5 h-5 rounded-full ring-4 ring-red-400 bg-white -left-2"></div>
                  </div>
                </div>
                {/* RIGHT */}
                <div className="w-1/3 "></div>
              </div>
              {/* EXPERIENCE LIST ITEM */}
              <div className="flex justify-between h-48">
                {/* LEFT */}
                <div className="w-1/3 "></div>
                {/* CENTER */}
                <div className="w-1/6 flex justify-center">
                  {/* LINE */}
                  <div className="w-1 h-full bg-gray-600 rounded relative">
                    {/* LINE CIRCLE */}
                    <div className="absolute w-5 h-5 rounded-full ring-4 ring-red-400 bg-white -left-2"></div>
                  </div>
                </div>
                {/* RIGHT */}
                <div className="w-1/3 ">
                  {/* JOB TITLE */}
                  <div className="bg-white p-3 font-semibold rounded-b-lg rounded-s-lg">
                    Designer and Frontend developer
                  </div>
                  {/* JOB DESC */}
                  <div className="p-3 text-sm italic">
                    Firstly, I Designed entire user panel interface. After that
                    developed the web-app using Next js framework.
                  </div>
                  {/* JOB DATE */}
                  <div className="p-3 text-red-400 text-sm font-semibold">
                    2019 - 2020{" "}
                  </div>
                  {/* JOB COMPANY */}
                  <div className="flex ">
                    {" "}
                    <div className="p-1 rounded bg-white max-h-7 text-sm font-semibold w-fit">
                      Giti Next
                    </div>
                    <div className="ml-2 w-10 h-10 ">
                      <Image src={GitiNext} alt="gitinext" />
                    </div>
                  </div>
                </div>
              </div>
              {/* EXPERIENCE LIST ITEM */}
              <div className="flex justify-between h-48">
                {/* LEFT */}
                <div className="w-1/3 ">
                  {/* JOB TITLE */}
                  <div className="bg-white p-3 font-semibold rounded-b-lg rounded-s-lg">
                    Freelancer{" "}
                  </div>
                  {/* JOB DESC */}
                  <div className="p-3 text-sm italic">
                    I provided web solutions, applying a range of technologies
                    to address client requirements.{" "}
                  </div>
                  {/* JOB DATE */}
                  <div className="p-3 text-red-400 text-sm font-semibold">
                    2018 - 2020{" "}
                  </div>
                </div>
                {/* CENTER */}
                <div className="w-1/6 flex justify-center">
                  {/* LINE */}
                  <div className="w-1 h-full bg-gray-600 rounded relative">
                    {/* LINE CIRCLE */}
                    <div className="absolute w-5 h-5 rounded-full ring-4 ring-red-400 bg-white -left-2"></div>
                  </div>
                </div>
                {/* RIGHT */}
                <div className="w-1/3 "></div>
              </div>
            </motion.div>
          </div>
        </div>
        {/* SVG CONTAINER */}
        <div className="hidden lg:block w-1/3 sticky top-0 z-30 xl:w-1/2">
          <Brain scrollYProgress={scrollYProgress} />
        </div>
      </div>
    </motion.div>
  );
};

export default AboutPage;
